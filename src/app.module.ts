import { Module } from '@nestjs/common';

import { Controller, Get, Req } from '@nestjs/common'
import { of, Observable } from 'rxjs'
import { Request } from 'express'

@Controller()
export class AppController {
  @Get()
  foo() {
    return { subscribe(){} }
  }

  @Get('reqroute')
  bar(@Req() req: Request): string | Observable<string> | Record<string, any> {
    // return 'hello world' // Works
    // return of('hello world') // Works
    return req.route // Error but should work too as it isn't an Observable
  }
}


@Module({
  controllers: [AppController],
})
export class AppModule {}
